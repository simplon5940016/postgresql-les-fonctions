# PostgreSQL - Les fonctions


## Écrire une fonction format_track_duration qui prend en entrée une durée en millisecondes et retourne ce temps formaté en 00m 00s.

Exemple : format_track_duration(192000) retourne "03m 12s".

```sql
CREATE OR REPLACE FUNCTION public.format_time(milliseconds integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
DECLARE
    total_seconds INTEGER;
    hours INTEGER;
    minutes INTEGER;
    seconds INTEGER;
    formatted_time TEXT;
BEGIN
    total_seconds := milliseconds / 1000;
    hours := total_seconds / 3600;
    minutes := (total_seconds % 3600) / 60;
    seconds := total_seconds % 60;
    formatted_time := LPAD(minutes::TEXT, 2, '0') || 'm ' ||
                      LPAD(seconds::TEXT, 2, '0') || 's';
    RETURN formatted_time;
END;
$function$
;
```



## Écrire une fonction get_album_title qui prend en entrée l'ID d'un album et retourne le titre de l'album.

Exemple : get_album_title(1) retourne "For Those About To Rock We Salute You".


```sql
CREATE OR REPLACE FUNCTION public.get_album_title(id integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
DECLARE
    title text;
BEGIN
    SELECT album.title INTO title FROM album WHERE album.album_id = id;
    RETURN title;
END;
$function$
;

```



## Écrire une fonction total_sales_by_customer qui prend en entrée l'ID d'un client et retourne le montant total des ventes pour ce client.

Exemple : total_sales_by_customer(1) retourne 39.62.

```sql
CREATE OR REPLACE FUNCTION public.total_sales_by_customer(id integer)
 RETURNS NUMERIC
 LANGUAGE plpgsql
AS $function$
DECLARE
    total_sales REAL;
BEGIN
    SELECT SUM(invoice.total) INTO total_sales
    FROM customer
    JOIN invoice ON customer.customer_id = invoice.customer_id
    WHERE customer.customer_id = id;
RETURN total_sales;
END;
$function$
;

```


## Écrire une fonction get_album_artist qui prend en entrée l'ID d'un album et retourne le nom de l'artiste de cet album.

Exemple : get_album_artist(1) retourne "AC/DC".

```sql
CREATE OR REPLACE FUNCTION public.get_album_artist(id integer)
 RETURNS TEXT
 LANGUAGE plpgsql
AS $function$
DECLARE
    artist_name TEXT;
BEGIN
	SELECT artist.name into artist_name
	FROM artist
	JOIN album ON album.artist_id = artist.artist_id
	WHERE album.album_id = id;
	RETURN artist_name;
END;
$function$
;
```

## Écrire une fonction list_album_tracks qui prend en entrée l'ID d'un album et retourne une table contenant les ID et les noms des pistes de cet album.

Exemple : list_album_tracks(1) retourne une table avec les colonnes track_id et name.

```sql
CREATE OR REPLACE FUNCTION public.list_album_tracks(id_album_search integer)
 RETURNS TABLE (
	id int,
	tracks_name varchar(200)
) 
LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
	RETURN QUERY
	SELECT track.track_id, track.name
	FROM track
	WHERE track.album_id = id_album_search;
END;
$function$
;

```


## Écrire une fonction total_artist_duration qui prend en entrée l'ID d'un artiste et retourne la durée totale de toutes les pistes de cet artiste en millisecondes.

Exemple : total_artist_duration(1) retourne 4853674.

```sql
CREATE OR REPLACE FUNCTION public.total_artist_duration(id_artist_search integer)
 RETURNS INTEGER
 LANGUAGE plpgsql
AS $function$
DECLARE
    total_duration INTEGER;
BEGIN
	SELECT SUM(track.milliseconds) INTO total_duration
	FROM track
	JOIN album ON track.album_id = album.album_id 
	WHERE album.artist_id = id_artist_search;
	RETURN total_duration;
END;
$function$
;

```